
(function() {
    function Bullet(url, size, position, speed, direction) {
        Entity.apply(this, arguments);
        this.direction = direction === 'down' ? 1 : -1;
    };

    Bullet.prototype = Object.create(Entity.prototype);
    Bullet.prototype.constructor = Bullet;

    Bullet.prototype.step = function(dt) {
        this.pos.y += this.direction * (this.speed * dt);
        return this.pos;
    };

    window.Bullet = Bullet;
})();