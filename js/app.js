
// A cross-browser requestAnimationFrame
// See https://hacks.mozilla.org/2011/08/animating-with-javascript-from-setinterval-to-requestanimationframe/
var requestAnimFrame = (function(){
    return window.requestAnimationFrame       ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame    ||
        window.oRequestAnimationFrame      ||
        window.msRequestAnimationFrame     ||
        function(callback){
            window.setTimeout(callback, 1000 / 60);
        };
})();

// Create the canvas
var canvas = document.createElement("canvas");
var ctx = canvas.getContext("2d");
canvas.width = 600;
canvas.height = 600;
document.body.appendChild(canvas);

// The main game loop
var lastTime;
function main() {
    var now = Date.now();
    var dt = (now - lastTime) / 1000.0;

    update(dt);
    render();

    lastTime = now;
    requestAnimFrame(main);
};

function init() {
    terrainPattern = ctx.createPattern(resources.get('img/terrain.png'), 'repeat');

    document.getElementById('play-again').addEventListener('click', function() {
        reset();
    });

    reset();
    lastTime = Date.now();
    main();
}

resources.load([
    'img/sprites.png',
    'img/ghost.png',
    'img/bullet.png',
    'img/person.png',
    'img/terrain.png'
]);
resources.onReady(init);

var bullets = [];
var enemiesBullets = [];
var enemies = [];
var scores = [];

var lastFire = Date.now();
var fireFrequency = 300;
var lastEnemyFire = Date.now();
var enemyFireFrequency = 1000;
var scoreVisible = 1000;
var gameTime = 0;
var isGameOver;
var terrainPattern;

var enemiesMoveDirection = 1;
var enemiesRows = 4;
var enemiesCols = 5;

var score = 0;
var scoreEl = document.getElementById('score');

// Speed in pixels per second
var playerSpeed = 200;
var bulletSpeed = 500;
var enemySpeed = 100;

// Game state
var player = new Entity(
    'img/person.png',
    { x: 39, y: 39 },
    { x: 0, y: 0 },
    playerSpeed
);

// Update game objects
function update(dt) {
    gameTime += dt;

    handleInput(dt);
    updateEntities(dt);
    if(Date.now() - lastEnemyFire > enemyFireFrequency && Math.random() > 0.8) {
        for (var i = enemies.length - 1; i >= 0; i -= 1) {
            for (var j = enemies[i].length - 1; j >= 0; j -= 1) {
                if (enemies[i][j] && (!enemies[i + 1] || !enemies[i + 1][j])) {
                    if (Math.random() > 0.5) {
                        var x = enemies[i][j].pos.x + enemies[i][j].size.x / 2;
                        var y = enemies[i][j].pos.y + enemies[i][j].size.y / 2;
                        enemiesBullets.push(new Bullet('img/bullet.png',
                            { x: 5, y: 9 }, { x: x, y: y }, bulletSpeed / 3, 'down'));
                        i = 0;
                        lastEnemyFire = Date.now();
                        break;
                   }
                }
            }
        }
    }

    checkCollisions();

    scoreEl.innerHTML = score;
};

function handleInput(dt) {
    if(input.isDown('LEFT') || input.isDown('a')) {
        player.step(-1, dt);
    }

    if(input.isDown('RIGHT') || input.isDown('d')) {
        player.step(1, dt);
    }

    if((input.isDown('SPACE') || input.isDown('UP') || input.isDown('w')) &&
       !isGameOver && Date.now() - lastFire > fireFrequency) {

        var x = player.pos.x + player.size.x / 2;
        var y = player.pos.y + player.size.y / 2;

        bullets.push(new Bullet('img/bullet.png',
            { x: 5, y: 9 }, { x: x, y: y }, bulletSpeed, 'up'));
        lastFire = Date.now();
    }
}

function updateEntities(dt) {
    // Update all the bullets
    for(var i=0; i<bullets.length; i++) {
        var bulletPos = bullets[i].step(dt);

        // Remove the bullet if it goes offscreen
        if(bulletPos.y < 0 || bulletPos.y > canvas.height ||
           bulletPos.x > canvas.width) {
            bullets.splice(i, 1);
            i--;
        }
    }
    for(var i=0; i<enemiesBullets.length; i++) {
        var bulletPos = enemiesBullets[i].step(dt);

        // Remove the bullet if it goes offscreen
        if(bulletPos.y < 0 || bulletPos.y > canvas.height ||
           bulletPos.x > canvas.width) {
            enemiesBullets.splice(i, 1);
            i--;
        }
    }

    var wallDetected = 0;
    for (var i = 0; i < enemies.length; i++) {
        for (var j = 0; j < enemies[i].length; j++) {
            if (!enemies[i][j]) {
                continue;
            }
            if (enemies[i][j].pos.x + enemies[i][j].size.x > canvas.width) {
                enemiesMoveDirection = -1;
                wallDetected = 1;
                i = enemies.length;
                break;
            } else if (enemies[i][j].pos.x <= 0) {
                enemiesMoveDirection = 1;
                wallDetected = 1;
                i = enemies.length;
                break;
            }
        }
    }
    for (var i = 0; i < enemies.length; i++) {
        for (var j = 0; j < enemies[i].length; j++) {
            if (!enemies[i][j]) {
                continue;
            }
            if (wallDetected) {
                enemies[i][j].stepForward(dt);
            }
            enemies[i][j].step(enemiesMoveDirection, dt);
        }
    }

    for(var i = 0; i < scores.length; i++) {
        if (Date.now() - scores[i].startTime >= scoreVisible) {
            scores.splice(i, 1);
            i--;
        }
    }
}

// Collisions

function collides(x, y, r, b, x2, y2, r2, b2) {
    return !(r <= x2 || x > r2 ||
             b <= y2 || y > b2);
}

function boxCollides(pos, size, pos2, size2) {
    return collides(pos.x, pos.y, pos.x + size.x, pos.y + size.y,
                    pos2.x, size2.x, pos2.x + size2.x, pos2.y + size2.y);
}

function checkCollisions() {
    checkPlayerBounds();
    var killedEnemies = 0;
    // Run collision detection for all enemies and bullets
    for(var i = 0; i < enemies.length; i++) {
        for(var j = 0; j < enemies[i].length; j++) {
            if (!enemies[i][j]) {
                killedEnemies += 1;
                continue;
            }
            var enemyPos = enemies[i][j].pos;
            var enemySize = enemies[i][j].size;

            for(var bulletIndex = 0; bulletIndex < bullets.length; bulletIndex ++) {
                var bulletPos = bullets[bulletIndex].pos;
                var bulletSize = bullets[bulletIndex].size;

                if(boxCollides(bulletPos, bulletSize, enemyPos, enemySize)) {
                    // Remove the enemy
                    var value = Math.round(enemies[i][j].value);
                    score += value;
                    scores.push(new Score(value, { x: enemyPos.x, y: enemyPos.y }));
                    console.log('boxCollides', enemies[i][j])
                    enemies[i][j] = null;
                    killedEnemies += 1;

                    // Remove the bullet and stop this iteration
                    bullets.splice(bulletIndex, 1);
                    break;
                }
            }

            if(boxCollides(player.pos, player.size, enemyPos, enemySize)) {
                console.log('enemies ate player');
                gameOver();
                i = enemies.length;
                break;
            }
        }
    }
    for(var bulletIndex = 0; bulletIndex < enemiesBullets.length; bulletIndex++) {
        var pos2 = enemiesBullets[bulletIndex].pos;
        var size2 = enemiesBullets[bulletIndex].size;

        if(boxCollides(pos2, size2, player.pos, player.size)) {
            console.log(pos2, size2);
            debugger;
            console.log('enemies killed player');
            enemiesBullets.splice(bulletIndex, 1);
            gameOver();
        }
    }

    if(killedEnemies === enemiesCols * enemiesRows) {
        console.log('killedEnemies', killedEnemies);
        gameOver();
    }
}

function checkPlayerBounds() {
    // Check bounds
    if(player.pos.x < 0) {
        player.pos.x = 0;
    }
    else if (player.pos.x > canvas.width - player.size.x) {
        player.pos.x = canvas.width - player.size.x;
    }

    if (player.pos.y < 0) {
        player.pos.y = 0;
    }
    else if (player.pos.y > canvas.height - player.size.y) {
        player.pos.y = canvas.height - player.size.y;
    }
}

// Draw everything
function render() {
    ctx.fillStyle = terrainPattern;
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    // Render the player if the game isn't over
    if(!isGameOver) {
        renderEntity(player);
    }
    renderEntities(bullets);
    renderEnemies(enemies);
    renderEntities(scores);
    renderEntities(enemiesBullets);
};

function renderEntities(list) {
    for(var i=0; i<list.length; i++) {
        renderEntity(list[i]);
    }    
}

function renderEnemies(list) {
    for(var i = 0; i < list.length; i++) {
        if (Array.isArray(list[i])) {
            renderEnemies(list[i]);
        } else if (list[i]) {
            renderEntity(list[i]);
        }
    }    
}

function generateEnemies() {
    for (var i = 0; i < enemiesRows; i++) {
        enemies.push([]);
        var enemySize = 40 + i * 5;
        for (var j = 0; j < enemiesCols; j++) {
            enemies[i].push(
                new Enemy(
                    'img/ghost.png',
                    { x: enemySize, y: enemySize },
                    { x: (j * 80) + 10, y: (i * 39) + 20 },
                    enemySpeed,
                    50 / (i + 1)
                )
            );
        }
    }
}

function renderEntity(entity) {
    entity.render(ctx);
}

// Game over
function gameOver() {
    document.getElementById('game-over').style.display = 'block';
    document.getElementById('game-over-overlay').style.display = 'block';
    isGameOver = true;
}

// Reset game to original state
function reset() {
    document.getElementById('game-over').style.display = 'none';
    document.getElementById('game-over-overlay').style.display = 'none';
    isGameOver = false;
    gameTime = 0;
    score = 0;

    enemies = [];
    generateEnemies();
    bullets = [];
    scores = [];

    player.pos.x = 50;
    player.pos.y = canvas.height;
};
