
(function() {
    function Entity(url, size, position, speed) {
        this.url = url;
        this.size = size;
        this.pos = { x: position.x, y: position.y };
        this.speed = speed;
    };

    Entity.prototype.render = function(ctx) {
        ctx.drawImage(resources.get(this.url),
              this.pos.x, this.pos.y,
              this.size.x, this.size.y);
    };

    Entity.prototype.step = function(direction, dt) {
        this.pos.x += (this.speed * dt) * direction;
        return this.pos;
    };

    window.Entity = Entity;
})();