
(function() {
    function Enemy(url, size, position, speed, value) {
        Entity.apply(this, arguments);
        this.value = value;
    };

    Enemy.prototype = Object.create(Entity.prototype);
    Enemy.prototype.constructor = Enemy;

    Enemy.prototype.stepForward = function(dt) {
        this.pos.y += this.speed * dt + 20;
        return this.pos;
    };

    window.Enemy = Enemy;
})();