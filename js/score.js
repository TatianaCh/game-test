
(function() {
    function Score(text, position) {
        this.text = text;
        this.pos = { x: position.x, y: position.y };
        this.startTime = Date.now();
    };

    Score.prototype.render = function(ctx) {
        ctx.font = '30px Arial';
        ctx.fillStyle = "red";
        ctx.fillText(this.text, this.pos.x, this.pos.y);
    };

    window.Score = Score;
})();